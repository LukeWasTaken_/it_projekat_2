<?php

const HOST = 'localhost';
const USERNAME = 'root';
const PASSWORD = '';
const DATABASE = 'it';

$sql = new mysqli(HOST, USERNAME, PASSWORD, DATABASE);

if ($sql->connect_error) {
    die('Connection failed: ' . $sql->connect_error);
}
?>