<?php

include_once 'db.php';

function createItem(string $title, float $price, string $image): void {
    global $sql;

    $stmt = $sql->prepare('INSERT INTO `games` (`title`,`price`, `image`) VALUES (?, ?, ?)');
    $stmt->bind_param('sds', $title, $price, $image);
    $stmt->execute();
};

function deleteItem(int $id): void {
    global $sql;

    $stmt = $sql->prepare('DELETE FROM `games` WHERE `id` = ?');
    $stmt->bind_param('i', $id);
    $stmt->execute();
}

function updateItem(int $id, string $title, float $price, string $image): void {
    global $sql;

    $stmt = $sql->prepare('UPDATE `games` SET `title` = ?, `price` = ?, `image` = ? WHERE `id` = ?');
    $stmt->bind_param('sdsi', $title, $price, $image, $id);
    $stmt->execute();
}

function getDirImages(): array {
    $dir = scandir('../assets/games');
    $images = [];

    foreach ($dir as $file) {
        if (!str_contains($file, '.jpg') && !str_contains($file, '.png')) continue;

        $images[] = $file;
    }

    return $images;
};