<?php
$url = basename($_SERVER['REQUEST_URI'], '.php');
$url = explode('.php', $url);

$page = $url[0];

$path = '';

if (str_contains($_SERVER['REQUEST_URI'], 'admin')) {
    $path = '../';
}
?>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="author" content="Luka Sabo"/>
        <meta name="description" content="GameShop - Sve igrice na jednom mestu"/>
        <meta name="robots" content="noindex"/>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;300;400;500;600;700&display=swap" rel="stylesheet">
        <link href="<?= $path ?>styles/main.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        />
        <title>GameShop</title>
    </head>
    <div class="page-wrapper">
        <header class="header-wrapper">
            <div class="header-container">
                <div style="display:flex;gap:60px;">
                    <a href="<?= $path ?>index.php" style="font-weight: bold; cursor: pointer">GameShop</a>
                    <div class="header-links-wrapper">
                        <a href="<?= $path ?>katalog.php" class="<?= $page === 'katalog' ? 'active' : null ?>">Katalog</a>
                        <a href="<?= $path ?>kontakt.php" class="<?= $page === 'kontakt' ? 'active' : null ?>">Kontakt</a>
                        <a href="<?= $path ?>o_nama.php" class="<?= $page === 'o_nama' ? 'active' : null ?>">O nama</a>
                    </div>
                </div>
                <div class="header-admin-links-wrapper">
                    <a style="font-size: 16px;" href="<?= $path ?>admin/admin.php" class="<?= $page === 'admin' ? 'active' : null ?> header-link">Admin</a>
                    <a target="_blank" href="https://gitlab.com/LukeWasTaken_/it_projekat_2">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-gitlab" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <path d="M21 14l-9 7l-9 -7l3 -11l3 7h6l3 -7z" />
                        </svg>
                    </a>
                </div>
            </div>
        </header>
        <body>
