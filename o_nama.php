<?php

include 'header.php';
include 'db.php';

$num = rand(10000, 50000);

?>

    <main class="main-container" style="align-items: flex-start">
        <div class="contact-container">
            <img src="assets/joystick.jpg" alt="joystick" width="300" style="align-self: center"/>

            <p>
                GameShop je prodavnica koja je već godinama uživala poverenje svojih korisnika kao pouzdan izvor za
                fizičke kopije najnovijih video igara. Osnovani smo s ciljem da stvorimo prostor gde igrači mogu
                jednostavno pronaći svoje omiljene naslove i dopuniti svoje gaming iskustvo. Sa strašću prema igrama kao
                pokretačkom snagom, GameShop je izrastao u etabliranu destinaciju za sve ljubitelje video igara.
            </p>

            <p>
                Ono što nas čini posebnim je naša posvećenost pružanju široke palete fizičkih kopija igara, prilagođenih
                raznolikim preferencama i platformama. Bez obzira da li ste zaljubljenik u akciju, avanturu, simulaciju
                ili sportske igre, GameShop nudi pažljivo odabrane naslove koji zadovoljavaju različite ukuse naše
                raznovrsne gaming zajednice.
            </p>
            <p>
                U svetu gde digitalna dostupnost postaje sveprisutna, GameShop ostaje posvećen pružanju onog što igrači
                vole - autentično iskustvo posedovanja fizičkih kopija igara. Naš tim stručnjaka pažljivo bira svaki
                naslov, obezbeđujući da svaka kopija zadovoljava standarde kvaliteta koje naši kupci očekuju.
            </p>

            <div class="about-game-container">
                <p class="about-game-count">
                    <?= number_format($num) ?>
                </p>
                <p>Igara prodato</p>
            </div>
        </div>
    </main>

<?php

include 'footer.php';
