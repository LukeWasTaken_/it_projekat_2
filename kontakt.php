<?php

include 'header.php';
include 'db.php';

?>

<main class="main-container">
    <div class="contact-container">
        <h2>Možete nas pronaći na jednoj od naših lokacija, ili nam poslati poruku!</h2>
        <div class="contact-wrapper">
        <div class="address-wrapper">
            <div class="address-container">
                <div class="map-icon-container">
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-map-pin-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path d="M12 18.5l-3 -1.5l-6 3v-13l6 -3l6 3l6 -3v7" />
                        <path d="M9 4v13" />
                        <path d="M15 7v5" />
                        <path d="M21.121 20.121a3 3 0 1 0 -4.242 0c.418 .419 1.125 1.045 2.121 1.879c1.051 -.89 1.759 -1.516 2.121 -1.879z" />
                        <path d="M19 18v.01" />
                    </svg>
                </div>
                <div>
                    <p>Adresa: <b>Marka Oreškovića 16, Subotica</b></p>
                    <p>Radno vreme: <b>08:00-20:00</b></p>
                </div>
            </div>
            <div class="address-container">
                <div class="map-icon-container">
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-map-pin-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path d="M12 18.5l-3 -1.5l-6 3v-13l6 -3l6 3l6 -3v7" />
                        <path d="M9 4v13" />
                        <path d="M15 7v5" />
                        <path d="M21.121 20.121a3 3 0 1 0 -4.242 0c.418 .419 1.125 1.045 2.121 1.879c1.051 -.89 1.759 -1.516 2.121 -1.879z" />
                        <path d="M19 18v.01" />
                    </svg>
                </div>
                <div>
                    <p>Adresa: <b>Školska 1, Novi Sad</b></p>
                    <p>Radno vreme: <b>07:30-15:30</b></p>
                </div>
            </div>
            <div class="address-container">
                <div class="map-icon-container">
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-map-pin-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path d="M12 18.5l-3 -1.5l-6 3v-13l6 -3l6 3l6 -3v7" />
                        <path d="M9 4v13" />
                        <path d="M15 7v5" />
                        <path d="M21.121 20.121a3 3 0 1 0 -4.242 0c.418 .419 1.125 1.045 2.121 1.879c1.051 -.89 1.759 -1.516 2.121 -1.879z" />
                        <path d="M19 18v.01" />
                    </svg>
                </div>
                <div>
                    <p>Adresa: <b>Bulevar Zorana Đinđića 152а, Beograd</b></p>
                    <p>Radno vreme: <b>08:00-17:00</b></p>
                </div>
            </div>
        </div>
        <form class="form-container">
            <div>
                <p>Ime</p>
                <input class="form-input"/>
            </div>
            <div>
                <p>Email</p>
                <input class="form-input"/>
            </div>
            <div>
                <p>Poruka</p>
                <textarea class="form-input" rows="10"></textarea>
            </div>
            <button class="btn-primary" type="submit">Pošalji poruku</button>
        </form>
        </div>
    </div>
</main>

<?php

include 'footer.php';
