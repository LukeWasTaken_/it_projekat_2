<?php

include 'header.php';
include 'db.php';

$search = $_GET['search'] ?? '';

if ($search == '') {
    $result = $sql->query('SELECT * FROM `games`');
} else {
    $stmt = $sql->prepare("SELECT * FROM `games` WHERE `title` LIKE ?");

    $term = '%' . $search . '%';

    $stmt->bind_param('s', $term);
    $stmt->execute();

    $result = $stmt->get_result();
}

?>

<main class="main-container" style="align-items: flex-start">
    <div class="catalog-wrapper">
        <form action="katalog.php" method="GET">
            <input placeholder="Pretraga..." name="search" value='<?= $_GET['search'] ?? "" ?>'/>
        </form>
        <div class="games-container">
            <?php
            while ($game = $result->fetch_assoc()) {
                echo "
                    <div class='game-container' style='width:100%'>
                        <div class='game-title' style='width:100%'>
                            <img src='assets/games/${game['image']}' alt='assets/${game['image']}' width='100%' style='border-radius: 8px'/>
                            <p style='text-align: left'>{$game['title']}</p>
                        </div>
        
                        <div class='price-container'>
                            <p style='color:var(--primary)'>{$game['price']}&euro;</p>
                            <a>
                                <button class='btn-primary'>
                                    Kupi
                                    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' stroke-width='2' stroke='currentColor' fill='none' stroke-linecap='round' stroke-linejoin='round'>
                                        <path stroke='none' d='M0 0h24v24H0z' fill='none' />
                                        <path d='M6 19m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0' />
                                        <path d='M17 19m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0' />
                                        <path d='M17 17h-11v-14h-2' />
                                        <path d='M6 5l14 1l-1 7h-13' />
                                    </svg>
                                </button>
                            </a>
                        </div>
                    </div>
                ";
            }

            ?>
        </div>
    </div>
</main>

<?php

include 'footer.php';
