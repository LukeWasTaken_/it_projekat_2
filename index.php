<?php

include 'header.php';
require 'db.php';

?>

<main class="main-container" style="justify-content: center">
    <div class="title-container">
        <h1>GameShop</h1>
        <h3>Tvoj svet igara, sada na dohvat jednog klika!</h3>
        <a href="katalog.php">
            <button class="btn-primary">
                Katalog
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-arrow-right" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                    <path d="M5 12l14 0" />
                    <path d="M13 18l6 -6" />
                    <path d="M13 6l6 6" />
                </svg>
            </button>
        </a>
    </div>
</main>

<?php

include 'footer.php';
