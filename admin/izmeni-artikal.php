<?php

include '../header.php';
include_once '../db.php';
include '../functions.php';

$id = $_POST['id'];

$stmt = $sql->prepare('SELECT `title`, `price`, `image` FROM `games` WHERE `id` = ?');
$stmt->bind_param('i', $id);
$stmt->execute();

$result = $stmt->get_result();

if (!$game = $result->fetch_assoc()) return;

$images = getDirImages();

?>

<main class="main-container" style="justify-content: center">
    <div class="new-article-container">
        <h2 style="text-align: center">Izmeni artikal</h2>
        <form class="form-container" method="POST" action="update-article.php">
            <div>
                <p>Naziv</p>
                <input class="form-input" name="title" value='<?= $game['title'] ?>' />
            </div>
            <div>
                <p>Cena</p>
                <input class="form-input" name="price" value="<?= $game['price'] ?>"/>
            </div>
            <div>
                <p>Slika</p>
                <select class="form-select" name="image">
                    <?php

                    foreach ($images as $image)
                        if ($image === $game['image'])
                            echo "<option selected>$image</option>";
                        else
                            echo "<option>$image</option>";

                    ?>
                </select>
            </div>
            <input type="hidden" value="<?= $id ?>" name="id" />
            <button class="btn-primary" type="submit">Potvrdi izmenu</button>
        </form>
    </div>
</main>

<?php

include '../footer.php';
