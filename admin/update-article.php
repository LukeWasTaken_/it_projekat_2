<?php

include_once '../functions.php';

$id = $_POST['id'];
$title = $_POST['title'];
$price = $_POST['price'];
$image = $_POST['image'];

updateItem($id, $title, $price, $image);

header('Location:admin.php');