<?php

include '../header.php';
include '../db.php';

$result = $sql->query('SELECT * FROM `games`');

?>

<main class="main-container" style="align-items: flex-start">
    <div class="admin-container">
        <div class="heading-wrapper">
            <h2>Artikli</h2>
            <a href="novi-artikal.php">
                <button class="btn-primary">
                    Novi artikal
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-plus" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path d="M12 5l0 14" />
                        <path d="M5 12l14 0" />
                    </svg>
                </button>
            </a>
        </div>
        <table>
            <thead>
                    <th style="text-align: left; padding-bottom: 8px;">Slika</th>
                    <th>Naziv</th>
                    <th>Cena</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php

            while ($game = $result->fetch_assoc())
                echo "
                    <tr>
                        <td style='text-align: left'><img src='../assets/games/{$game['image']}' alt='{$game['image']}' width='100px'/></td>
                        <td>{$game['title']}</td>
                        <td>{$game['price']}&euro;</td>
                        <td>
                            <div class='table-actions'>
                                <form method='POST' action='izmeni-artikal.php'>
                                    <input type='hidden' value='{$game['id']}' name='id' />
                                    <button class='btn-primary'>
                                        <svg xmlns='http://www.w3.org/2000/svg' class='icon icon-tabler icon-tabler-edit' width='24' height='24' viewBox='0 0 24 24' stroke-width='2' stroke='currentColor' fill='none' stroke-linecap='round' stroke-linejoin='round'>
                                          <path stroke='none' d='M0 0h24v24H0z' fill='none' />
                                          <path d='M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1' />
                                          <path d='M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z' />
                                          <path d='M16 5l3 3' />
                                        </svg>
                                    </button>
                                </form>
                                <form method='POST' action='delete-article.php'>
                                    <input name='id' value='{$game['id']}' type='hidden' />
                                    <button class='btn-primary' type='submit'>
                                        <svg xmlns='http://www.w3.org/2000/svg' class='icon icon-tabler icon-tabler-trash' width='24' height='24' viewBox='0 0 24 24' stroke-width='2' stroke='currentColor' fill='none' stroke-linecap='round' stroke-linejoin='round'>
                                          <path stroke='none' d='M0 0h24v24H0z' fill='none' />
                                          <path d='M4 7l16 0' />
                                          <path d='M10 11l0 6' />
                                          <path d='M14 11l0 6' />
                                          <path d='M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12' />
                                          <path d='M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3' />
                                        </svg>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                ";
            ?>
            </tbody>
        </table>
    </div>
</main>

<?php

include '../footer.php';
