<?php

include '../header.php';
include '../functions.php';

$images = getDirImages();

?>

<main class="main-container" style="justify-content: center">
    <div class="new-article-container">
        <h2 style="text-align: center">Novi artikal</h2>
        <form class="form-container" method="POST" action="create-article.php">
            <div>
                <p>Naziv</p>
                <input class="form-input" name="title"/>
            </div>
            <div>
                <p>Cena</p>
                <input class="form-input" name="price"/>
            </div>
            <div>
                <p>Slika</p>
                <select class="form-select" name="image">
                    <?php

                    foreach ($images as $image)
                        echo "<option>$image</option>";

                    ?>
                </select>
            </div>
            <button class="btn-primary" type="submit">Dodaj artikal</button>
        </form>
    </div>
</main>

<?php

include '../footer.php';
